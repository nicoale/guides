# User is able to withdraw

The user should be able to withdraw

For withdrawal to occur the phone number provided must exist, correct pin should be provided, i.e
pin and phone_number should belong to the same user and agent number should also be provided,for now 
agent should be any digit based characters of length 6 to 9 characters.


If account(phone number) do not exist  the user should be informed 

If pin do not match given phone number the user should be informed 

If certain amount is withdrawn balance should decrease by that amount for instance if balance was 50.0 and 20.0 is withdrawn, the new balance is 30.0

User should be given feedback on successfuly withdrawal for example, "You have successfully withdrawn from 30.00.Agent 1234567, your new balance is 50.00"